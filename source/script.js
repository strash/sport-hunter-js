(() => {
  let DATA = {};
  let LANG = '';

  const NODES = {
    header: document.getElementById('header'),
    burger: document.getElementById('burger'),
    menu: document.getElementById('menu'),
    menuList: document.getElementById('menu-list'),
    closer: document.getElementById('closer'),
    wallpapers: document.getElementById('page-wallpapers'),
    wallpaper: document.getElementById('page-wallpaper'),
    forecasts: document.getElementById('page-forecasts'),
    forecast: document.getElementById('page-forecast'),
    about: document.getElementById('page-about')
  };

  // установка локали
  function setLanguage() {
    LANG = navigator.language.toLowerCase().slice(0, 2);
  }

  function getLocalData() {
    return LANG == 'ru' ? DATA.ru : DATA.en;
  }

  // получение данных
  async function getData() {
    return await fetch(`data.json`).then(res => res.json());
  }

  // установка картинок
  function setImages() {
    for (let i = 0; i < DATA.image.length; i++) {
      let img = document.createElement('div');
      img.className = 'preview';
      img.id = `image-${i}`;
      img.setAttribute('style', `background-image: url("${DATA.image[i]}");`);
      NODES.wallpapers.appendChild(img);
    }
  }

  // установка текста по локали
  function setData() {
    // установка заголовка
    NODES.header.textContent = getLocalData().header.wallpapers;
    // установка списка меню
    for (let i = 0; i < NODES.menuList.children.length; i++) {
      NODES.menuList.children[i].textContent = getLocalData().header[NODES.menuList.children[i].id];
    }
    // установка списка форкастов
    for (let i = 0; i < getLocalData().forecasts.length; i++) {
      let forcast = document.createElement('li');
      forcast.dataset.data = JSON.stringify(getLocalData().forecasts[i]);
      let title = document.createElement('h3');
      title.textContent = getLocalData().forecasts[i].title;
      let p = document.createElement('p');
      p.textContent = getLocalData().forecasts[i].body_preview;
      let label = document.createElement('div');
      label.className = 'label';
      let hot = document.createElement('span');
      hot.textContent = 'hot';
      let icon = document.createElement('img');
      icon.src = 'assets/icon/hot.svg';
      icon.setAttribute('alt', 'hot');
      NODES.forecasts.children[0].appendChild(forcast);
      forcast.appendChild(title);
      forcast.appendChild(p);
      forcast.appendChild(label);
      label.appendChild(hot);
      label.appendChild(icon);
    }
    // установка эбаут
    for (let i = 0; i < getLocalData().about.length; i++) {
      let p = document.createElement('p');
      p.textContent = getLocalData().about[i];
      NODES.about.appendChild(p);
    }
  }

  // открытие экрана
  function openPage(pageName) {
    NODES.wallpapers.style.display = 'none';
    NODES.wallpaper.style.display = 'none';
    NODES.forecasts.style.display = 'none';
    NODES.forecast.style.display = 'none';
    NODES.about.style.display = 'none';
    NODES[pageName].style.display = NODES[pageName].dataset.display;
  }

  // открытие меню
  function openMenu() {
    NODES.menu.style.display = NODES.menu.dataset.display;
  }

  // закрытие меню
  function closeMenu() {
    NODES.menu.style.display = 'none';
  }

  // открытие страницы через меню
  function setMenu(e) {
    let target = e.target;
    if (target.tagName == 'UL') return;
    while (target.tagName !== 'LI') target = target.parentNode;
    // подсветка пункта меню
    for (let i = 0; i < NODES.menuList.children.length; i++) {
      if (NODES.menuList.children[i] == target) NODES.menuList.children[i].classList.add('active');
      else NODES.menuList.children[i].classList.remove('active');
    }
    // заскрытие меню
    closeMenu();
    // открытие выбранной страницы
    openPage(target.id);
    NODES.header.textContent = getLocalData().header[target.id];
  }

  // открытие экрана с обоиной
  function openImage(e) {
    let target = e.target;
    while (!target.classList.contains('preview')) target = target.parentNode;
    let id = +target.id.slice(6);
    NODES.wallpaper.style.backgroundImage = `url('${DATA.image[id]}')`;
    NODES.wallpaper.children[0].href = `${DATA.image[id]}`;
    NODES.wallpapers.style.display = 'none';
    NODES.wallpaper.style.display = 'block';
  }

  // открытие форкаста
  function openForecast(e) {
    NODES.forecast.innerHTML = '';
    let target = e.target;
    if (target.tagName == 'UL') return;
    while (target.tagName !== 'LI') target = target.parentNode;
    let data = JSON.parse(target.dataset.data);
    let title = document.createElement('h2');
    title.textContent = data.title;
    NODES.forecast.appendChild(title);
    for (let i = 0; i < data.body.length; i++) {
      let p = document.createElement('p');
      p.textContent = data.body[i];
      NODES.forecast.appendChild(p);
    }
    openPage('forecast');
  }

  // инициализация
  async function init() {
    setLanguage();
    DATA = await getData();
    setImages();
    setData();
    NODES.burger.addEventListener('click', openMenu);
    NODES.closer.addEventListener('click', closeMenu);
    NODES.menuList.addEventListener('click', setMenu);
    NODES.wallpapers.addEventListener('click', openImage);
    NODES.forecasts.children[0].addEventListener('click', openForecast);
    // console.log(DATA);
  }

  init();
})();
